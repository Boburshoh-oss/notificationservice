from django.urls import path, include
from .views import ClientViewSet, MailingViewSet, MessageViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'messages', MessageViewSet)
router.register(r'mailings', MailingViewSet)
urlpatterns = [
    
    path('', include(router.urls)),   
]
