from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q
from django.utils import timezone
from .models import Mailing, Client, Message
from .tasks import send_message


@receiver(post_save, sender=Mailing, dispatch_uid="create_message")
def create_message(sender, instance, created, **kwargs):
    if created:
        clients = Client.objects.filter(Q(mobile_operator_code=instance.mobile_operator_code) |
                                        Q(tag=instance.tag)).all()
        
        for client in clients:
            Message.objects.create(
                sending_status="No sent",
                client_id=client.id,
                mailing_id=instance.id
            )
            message = Message.objects.filter(mailing_id=instance.id, client_id=client.id).first()
            data = {
                'id': message.id,
                "phone": client.phone_number,
                "text": instance.text
            }
            client_id = client.id
            mailing_id = instance.id
            
            now = timezone.now()
            
            if instance.date_start <= now <= instance.date_end:
                send_message.apply_async((data, client_id, mailing_id),
                                         expires=instance.date_end)
            else:
                send_message.apply_async((data, client_id, mailing_id),
                                         eta=instance.date_start, expires=instance.date_end)

            