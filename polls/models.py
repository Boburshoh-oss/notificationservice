from django.db import models
import pytz
from django.core.validators import RegexValidator



phone_regex = RegexValidator(regex=r'^7\d{10}$',
                             message="The client's phone number in the format 7XXXXXXXXXX (X - number from 0 to 9)")


class Mailing(models.Model):
    date_start = models.DateTimeField(verbose_name='Mailing start')
    date_end = models.DateTimeField(verbose_name='End of mailing')
    time_start = models.TimeField(verbose_name='Start time to send message')
    time_end = models.TimeField(verbose_name='End time to send message')
    text = models.TextField(max_length=255)
    tag = models.CharField(
        max_length=100, verbose_name='Search by tags', blank=True, null=True)
    mobile_operator_code = models.CharField(verbose_name='Search by mobile operator code',
                                            max_length=3, blank=True, null=True)

    def __str__(self) -> str:
        return self.text


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(verbose_name='Phone number', validators=[
                                    phone_regex], unique=True, max_length=11)
    mobile_operator_code = models.CharField(
        verbose_name='Mobile operator code', max_length=3, editable=False)
    tag = models.CharField(verbose_name='Search tags',
                           max_length=100, blank=True)
    timezone = models.CharField(
        verbose_name='Time zone', max_length=200, choices=TIMEZONES, default='UTC')

    def save(self, *args, **kwargs):
        self.mobile_operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'Client {self.id} with number {self.phone_number}'


class Message(models.Model):

    STATUS_CHOICES = [
        ("sent", "Sent"),
        ("no sent", "No sent"),
    ]
    
    time_create = models.DateTimeField(auto_now_add=True)
    sending_status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='clients')

    def __str__(self):
        return f'{self.mailing} for {self.client}'


