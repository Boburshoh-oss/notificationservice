from django.contrib import admin
from .models import Message, Mailing, Client
# Register your models here.
admin.site.register(Message)
admin.site.register(Mailing)
admin.site.register(Client)