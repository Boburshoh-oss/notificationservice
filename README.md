# NotificationService

# Сервис уведомлений

Сервис разработан на django rest framework с celery и flower


## Установка и запуск

1. Склонировать репозиторий с Github:

````
git clone https://gitlab.com/Boburshoh-oss/notificationservice.git
````
2. Перейти в директорию проекта

3. Создать виртуальное окружение:

````
python -m venv venv
````

4. Активировать окружение: 

````
source \venv\bin\activate
````
5. В файле .evn заполнить необходимые данные: ```TOKEN = '<your token>'```
 
6. Установка зависимостей:

```
pip install -r requirements.txt
```

7. Создать и применить миграции в базу данных:
```
python manage.py makemigrations
python manage.py migrate
```
8. Запустить сервер
```
python manage.py runserver 8010
```
9. Запустить celery
```
celery -A config worker -l info
```
10. Запустить flower

```
celery -A config flower --port=5557
```
***

***
## Установка проекта с помощью docker-compose


1. Склонировать репозиторий с Github
```
git clone https://gitlab.com/Boburshoh-oss/notificationservice.git
```
2. Перейти в директорию проекта
3. В файле .evn заполнить необходимые данные: ```TOKEN = '<your token>'```
4. построить и запустить проект
```
docker-compose up --build 
```
5. Запустить контейнеры 
``` 
sudo docker-compose up -d
 ```
6. Остановка работы контейнеров 
```
sudo docker-compose stop
```
***
```http://0.0.0.0:8010/api/``` - api проекта

```http://0.0.0.0:8010/api/clients/``` - клиенты

```http://0.0.0.0:8010/api/mailings/``` - рассылки

```http://0.0.0.0:8010/api/mailings/full_info/``` - общая статистика по всем рассылкам

```http://0.0.0.0:8010/api/mailings/<pk>/detail_info/``` - детальная статистика по конкретной рассылке

```http://0.0.0.0:8010/api/messages/``` - сообщения

конечная точка для получения необходимого токена доступа ```http://localhost:8010/token/```  и  ```http://localhost:8010/token/refresh/```

```http://0.0.0.0:8010/docs/``` - docs проекта

```http://0.0.0.0:5557/``` - celery flower

***

**Техзадание:** 
[https://www.craft.do/s/n6OVYFVUpq0o6L](https://www.craft.do/s/n6OVYFVUpq0o6L)

